import json
from dateutil.parser import isoparse
from datetime import datetime
import sys

ticketFile = open('tickets.json', 'r')
ticketData = json.load(ticketFile)
ticketFile.close()
organizationFile = open('organizations.json', 'r')
organiaztionData = json.load(organizationFile)
organizationFile.close()
userFile = open('users.json', 'r')
userData = json.load(userFile)
userFile.close()


def menu():
    print('''
    \n\tSelect search options:
        \t*Press 1 to search Zendesk
        \t*Press 2 to view a list of searchable fields
        \t*Type 'quit' to exit''')
    return input("\t\t:")


def listOfsearchFields(dataSource):
    fields = set()
    for data in dataSource:
        fields = fields.union({field for field in data.keys()})
    fields = list(fields)
    fields.sort()
    return fields

def printSearchFields(nameOffile, fields):
    print('{0}'.format('*'*50))
    print('Search {0} with'.format(nameOffile.title()))
    for field in fields:
        print(field)
        
def getUsersOfOrganization(organization):
    users = []
    for user in userData:
        organization_id = user.get("organization_id")
        if(organization_id and organization_id == organization):
            users.append(user)
    return users


def getTicketsOfOrganization(organization):
    tickets = []
    for ticket in ticketData:
        organization_id = ticket.get("organization_id")
        if(organization_id and organization_id == organization):
            tickets.append(ticket)
    return tickets

def getTicketAssigneeOrSubmitter(_id):
    for user in userData:
        userId = user.get('_id')
        if(userId and userId == _id):
            return user

def getOrganizationById(_id):
    for organization in organiaztionData:
        orgId = organization.get('_id')
        if(orgId and orgId == _id):
            return organization

def getSubmittedTickets(_id):
    tickets = []
    for ticket in ticketData:
        submitterId = ticket.get("submitter_id")
        if(submitterId and submitterId == _id):
            tickets.append(ticket)
    return tickets

def getAssignedTickets(_id):
    tickets = []
    for ticket in ticketData:
        assignedId = ticket.get("assignee_id")
        if(assignedId and assignedId == _id):
            tickets.append(ticket)
    return tickets

def validateTimeFormat(value):
    try:
        datetime.strptime(value,'%Y-%m-%d')
        isoparse(value)
        return True
    except:
        return False

def filterByTerm(dataSource, searchTerm, searchValue):
    filteredData = []
    for data in dataSource:
        termValue = data.get(searchTerm)
        if(str(termValue) and searchValue!=""):           
            if (        
                searchTerm in
                ["domain_names", "tags"] and
                searchValue.lower() in
                list(map(lambda x: x.lower(), termValue))):
                filteredData.append(data)
            elif (searchTerm in 
                  ['domain_names','tags'] and searchValue==""):
                filteredData.append(data)
            elif (
                str(termValue).lower() == str(searchValue).lower()):
                filteredData.append(data)                
            elif (
                ('_at'in searchTerm) and
                str(isoparse("".join(termValue.split(sep=" "))).date()) == searchValue):
                filteredData.append(data)
        elif(searchTerm in listOfsearchFields(dataSource) 
             and searchValue==""):
            filteredData = dataSource
    return filteredData
    
    

def search(selection, searchTerm, searchValue):
    if(selection == '1'):
        users = filterByTerm(userData,
                             searchTerm,
                             searchValue)
        if(users):
            for user in users:
                user['organization'] = getOrganizationById(
                    user.get('organization_id'))
                user['submitted_Tickets'] = getSubmittedTickets(user.get('_id'))
                user['assigned_Tickets'] = getAssignedTickets(user.get('_id'))
                print(json.dumps(user, indent=4, sort_keys=True))
                
        else:
            print('No results found')                
    elif(selection == '2'):
        tickets = filterByTerm(ticketData,
                               searchTerm,
                               searchValue)
        if(tickets):
            for ticket in tickets:
                ticket['assignee'] = getTicketAssigneeOrSubmitter(ticket.get('assignee_id'))
                ticket['submitter'] = getTicketAssigneeOrSubmitter(ticket.get('submitter_id'))
                ticket['organization'] = getOrganizationById(ticket.get('organization_id'))
                print(json.dumps(ticket, indent=4, sort_keys=True))
        else:
            print('No results found')
    else:
        organizations = filterByTerm(organiaztionData,
                                     searchTerm,
                                     searchValue)
        if(organizations):
            for organization in organizations:
                organizationId = organization.get('_id')
                if(organizationId):
                    tickets = getTicketsOfOrganization(organizationId)
                    users = getUsersOfOrganization(organizationId)
                    organization['tickets'] = tickets
                    organization['users'] = users
                    print(json.dumps(organization, indent=4, sort_keys=True))
        else:
            print('No results found')


def app():
    value = input(
        "Type 'quit' to exit at any time, Press 'Enter' to continue\n")
    if(value == ""):
        option = menu()
        while(option != 'quit'):
            if(option == '1'):
                selection = input('Select 1) Users or 2) Tickets or 3) Organizations\n')
                selection = selection.strip()
                if(selection in ['1', '2', '3']):
                    searchTerm = input('Enter search term\n')
                    searchTerm = searchTerm.strip()
                    if('_at' in searchTerm):
                        print('please enter date in iso format YYYY-MM-DD')
                    searchValue = input('Enter search value\n')
                    searchValue = searchValue.strip()
                    if('_at' in searchTerm):
                        searchValue = str(isoparse(searchValue).date()) if validateTimeFormat(searchValue) else searchValue

                    print(
                        "Searching {0} for {1} with a value of {2}"
                        .format({"1":"Users","2":"Tickets","3":"Organization"}.get(selection),searchTerm,searchValue))
                    search(selection, searchTerm, searchValue)
                option = menu()
            elif(option == '2'):
                printSearchFields('organizations', listOfsearchFields(organiaztionData))
                printSearchFields('tickets', listOfsearchFields(ticketData))
                printSearchFields('users', listOfsearchFields(userData))
                option = menu()
            elif(option!='quit'):
                option = menu()                
    elif(value != "quit"):
        app()

if __name__ == "__main__":
    try:
        app()
    except:
        print(sys.exc_info()[0],'occured')
    
